import React from 'react';
import axios from 'axios';
import url from '../config.js'
import { Table, Button, Containter, Modal, ModalBody, ModalHeader, FormGroup, ModalFooter } from 'reactstrap';

class ListContact extends React.Component {

  constructor(props) {
      super(props);
      this.state = { 
        contactos : [],
        showModal : false,
        id : 0,
        nombre : "",
        apellido : "",
        telefono : ""
      };
  }

  componentDidMount() {
    this.petitionID = setInterval(
      () => this.petitionGET(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.petitionGET);
  }

  petitionGET() {
    axios.get(`${url}/contactos`)
    .then(res => {
      const contactos = res.data;
      this.setState({ contactos });
    })
  }

  editarContacto(id) {
    const listContact = this.state.contactos;
    listContact.forEach(element => {
      if (element.id==id) {
        this.setState({ 
          id : element.id,
          nombre : element.nombre,
          apellido : element.apellido,
          telefono : element.telefono
        });
      }      
    });
    this.setState({ showModal: true });
  }

  closeModal() {
    this.setState({ showModal : false });
  }

  updateContacto() {

    axios.put(`${url}/contactos/${this.state.id}`, 
      { "nombre" : this.state.nombre,
        "apellido" : this.state.apellido,
        "telefono" : this.state.telefono
      })
    .then(res => {
        console.log(res);
        console.log(res.data);
      })
    this.setState({ showModal: false });
  }

  borrarContacto(id) {
    axios.delete(`${url}/contactos/${id}`)
    .then(res => {
        console.log(res);
        console.log(res.data);
      })
  }

  nombreHandler(e) {
    this.setState({ nombre: e.target.value });
  }

  apellidoHandler(e) {
    this.setState({ apellido: e.target.value });
  }

  telefonoHandler(e) {
    this.setState({ telefono: e.target.value });
  }

  render() {
    return(
        <React.Fragment>
          <Table>
            <thead>
              <th>Id</th>
              <th>Nombre</th>
              <th>Apellido</th>
              <th>Telefono</th>
              <th>Actions</th>
            </thead>
            <tbody>
              {this.state.contactos.map((elemento)=>(
                <tr>
                  <td>{elemento.id}</td>
                  <td>{elemento.nombre} </td>
                  <td>{elemento.apellido}</td>
                  <td>{elemento.telefono}</td>                  
                  <td> 
                    <Button color="primary" onClick={ ()=>this.editarContacto(elemento.id) }>Editar</Button>{"   "}
                    <Button color="danger" onClick={ ()=>this.borrarContacto(elemento.id) }>Eliminar</Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>

          <Modal isOpen={this.state.showModal} >
            <ModalHeader><h1>Editar contacto</h1></ModalHeader>
            <ModalBody>
              <FormGroup>
                <label>Nombre:</label>
                <input id="nombre" className="form-control" name="nombre" type="text" value={this.state.nombre} onChange={(e) => this.nombreHandler(e)} />
              </FormGroup>
              <FormGroup>
                <label>Apellido:</label>
                <input id="apellido" className="form-control" name="apellido" type="text" value={this.state.apellido} onChange={(e) => this.apellidoHandler(e)} />
              </FormGroup>
              <FormGroup>
                <label>Telefono:</label>
                <input id="telefono" className="form-control" name="telefono" type="text" value={this.state.telefono} onChange={(e) => this.telefonoHandler(e)} />
              </FormGroup>
            </ModalBody>
            <ModalFooter>
                <Button color='primary' onClick={ ()=>this.updateContacto() }>Actualizar</Button>{' '}
                <Button color='secondary' onClick={ ()=>this.closeModal() } >Cancel</Button>
            </ModalFooter>
        </Modal>


        </React.Fragment>
    );
  }

}

export default ListContact;