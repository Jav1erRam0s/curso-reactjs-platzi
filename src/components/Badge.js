import React from 'react';

import imgLogo from '../images/Logo UNGS.png';
import imgIcon from '../images/The1984.png';
import './styles/Badge.css';
import Clock from '../components/Clock.js';

class Badge extends React.Component {
     render() {

        return (
            <React.Fragment>
                <Clock/>
                <div className="Badge">

                    <div className="Badge__header">
                        <img src={imgLogo} alt="Logo de la conferencia" />
                    </div>

                    <div className="Badge__section-name">
                        <img className="Badge__avatar" src={this.props.avatarUrl} alt="Avatar" />
                        <h1>{this.props.firstName} <br/>{this.props.lastName}</h1>
                    </div>

                    <div className="Badge__section-info">
                        <p>{this.props.jobTitle}</p>
                        <p>@{this.props.twitter}</p>
                    </div>

                    <div className="Badge__footer">
                        #platzionf
                    </div>
                </div>
            </React.Fragment>
        )
     }
}

export default Badge;