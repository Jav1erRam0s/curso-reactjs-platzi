
****************************************************************************************************************************************************
*************************************************************  ¿ Que es un componente ?  ***********************************************************
****************************************************************************************************************************************************

Los componentes son bloques de construccion. Las aplicaciones hechas en React son como figuras de Lego. Junta varias piezas (los componentes) y 
puedes construir un website tan pequeño o tan grande como quieras.
En el site, lo componentes van a hacer barras de busqueda, enlaces, encabezado, header, footer, etc.

* 'Componente' vs 'Elemento'

Un elemento es a un objeto como un  componente es a una clase. Si el elemento fuera una casa, el componente seria los planos para hacer esa casa.

* Identificacion de componentes

Dos preguntas guias:
    - ¿Que elementos se repiten?
        .Elementos en una lista.
        .Elementos que comparten aspectos visuales y su funcionalidad.
        Ej. El listado de peliculas de youtube, netflix, etc.
            Un switch, comparten aspecto visual y su funcionalidad.

    - ¿Que elementos cumplen una funcion muy especifica?
        .Sirven para encapsular logica.
        .Permiten juntar muchos comportamientos y aspectos visuales en un solo lugar.
        Ej. La barra de busqueda. Es mas complejo que el componente que se repite por que interactua mucho con el usuario, salen ressultados, hacen 
        peticiones, regresa links, le das click y te vas a otra pagina. Es como encapsular trabajo dentro de una funcion, en nuestro caso componente.

"Identificar componentes es una habilidad esencial para poder desarrollar aplicaciones en React"

****************************************************************************************************************************************************
**********************************************************************  Props  *********************************************************************
****************************************************************************************************************************************************

Son atributos para hacer nuestro componentes reutilizable. En pocas palabras son atributos de nuestro componente. Un analogo a esto son los 
atributos de los elementos en HTML. Por ej. el elemento <a>...</a> tiene un atributo href que es la url del elemento/enlace. Otro ejemplo tambien es
el elemento <img>...</img> que tiene src (atributo/propiedad/argumentos).

****************************************************************************************************************************************************
**********************************************************************  State  *********************************************************************
****************************************************************************************************************************************************

Los state son objetos planos de JavaScript. Ambos influyen en el resultado del render pero... se diferencian en lo siguiente:
Los 'props' se pasan al componente (similar al parametro de una funcion), por otro lado los 'states' se administran dentro del componente (similar a
las variables declaradas dentro de una funcion).
Tanto this.props como this.state representan los valores renderizados, es decir, lo que hay actualmente en la pantalla.

Otra caracteristica importante de esto es que mientras los props son inmutables, los estados pueden cambiar, son mutables.

Entonces tenemos que, por un lado los estados actuan en el contexto del componente y por el otro, las propiedades crean una instancia del componente
cuando le pasas un nuevo valor desde el componente padre.

Los valores de las propiedades los pasas de padres a hijos y los valores de los estados los defines en el componente, no se inician en el componente
padre y no puedes llamar setState() en el render().

* ¿ Cuando usar state o estados en un componente ?

Rta.: Como bien dijimos las propiedades son inmutables. La tipica utilizacion de los estados seria en un componente Reloj por ej. ya que necesitas
actualizar de manera periodica la vista con los segundos.

* ¿ Como acceder a los estados y a los valores ?

Rta.: Para acceder al estado, debes hacerlo por el nombre del estado, que es una propiedad de objeto del objeto this.state. Algo muy parecido a
como se hace con las props. Ej. this.state.{nombre_del_estado}.

* ¿ Como establezco los valores iniciales ?

Para inicial el estado, lo haces mediante 'this.state' y este debe ser un objeto que esta en el 'constructor()'. Ademas, es importante invocar a 
'super()' con los props. Si no, no funciona la logica en el padre. Es una forma de asegurar que el constructor() del padre se ejecute.

Si no inicializas el estadoy no enlazas los metodos, no necesitas implementar un constructor para tu componente React.

El constructor para un componente React es llamado antes de ser montado. Al implementar el constructor para una subclase React.Component, deberias
llamar a super(props) antes que cualquier otra instruccion. De otra forma, this.props no estara definido en el constructor, lo que puede ocacionar
errores.

Normalmente, los constructores de React solo se utilizan para dos propositos:

. Para inicializar un estado local asignando un objeto al this.state.
. Para enlazar manejadores de eventos a una instancia.

* ¿ Como actualizo los estados ?

Actualizar los estados es tan sencilllo como declarar this.setState(data, callback). Solomente debes recordar que React fusiona los datos con los 
estados existentes y posteriormente llama el 'render()'.

Ademas hay que tener en cuenta que setState() funciona de forma asincrona, es decir - this.state no esta inmediatamente disponible despues de llamar
setState() - , por eso es importante tener el callback en setState(). Es una forma de asegurar que el nuevo estado esta disponible.

Por cierto, cambiar el estado sin utilizar setState() esta considerado antipatron.

* ¿ Que mas deberia saber ?

setState() actualiza solamente el estado que quieres. No siempre actualiza todo el objeto. Por ejemplo, si en un objeto tienes diferentes estados, 
como el nombre, apellido, edad y solamente quieres actualizar la edad, lo único que tienes que hacer es algo como this.setState({edad:24}); Los 
demás valores quedarán igual.

----------------------------------------------------------------------------------------------------------------------------------------------------
- Recordemos:
    * Programacion Asincrona: Se refiere a la ejecucion de procesos de manera simultanea. Algunas de las instrucciones se ejecutan a destiempo.
    * Programacion Sincrona: Se refiere a la ejecucion de un solo proceso de manera simultanea. Cada instrucion se ejecuta en secuencia hasta 
    terminar.

Nota: JavaScript es sincrono pero NodoJS es asincriono, V8 el motor de JS que utiliza node convierte el codigo JS en lenguaje maquina que puede ser
interpretado por el CPU.
----------------------------------------------------------------------------------------------------------------------------------------------------

Actualmente, setState es asincrono dentro de los controladores de los eventos. Esto garantiza, por ejemplo, que si Parent y Child llaman a setState 
durante un evento de click, Child no se renderiza dos veces. En su lugar, React “vacía” las actualizaciones del estado al final del evento del 
navegador. Esto se traduce en mejoras significativas de rendimiento en aplicaciones más grandes.
